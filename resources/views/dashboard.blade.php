@extends('app')

@section('content')

<div class="row" id="crud">
	<div class="col-sm-12">
		<div class="page-header h1">CRUD Laravel - Vue</div>
	</div>
	<div class="col-sm-7">
		<a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#create">Nueva Tarea</a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Tarea</th>
					<th colspan="2">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="keep in keeps":key="keep.id">
					<td width="10px">@{{keep.id}}</td>
					<td>@{{keep.keep}}</td>
					<td width="10px"><a href="#" class="btn btn-warning btn-sm" v-on:click.prevent="editKeeps(keep)">Editar</a></td>

					<td width="10px"><a href="#" class="btn btn-danger btn-sm" v-on:click.prevent="deleteKeeps(keep)">Eliminar</a></td>

				</tr>
			</tbody>
		</table>
		@include('create')
		@include('edit')
	</div>
	<div class="col-sm-5">
		<pre>
			@{{ $data }}
		</pre>
	</div>
</div>

@endsection